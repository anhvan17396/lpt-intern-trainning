package exercise.exercise5;

import java.util.Scanner;

public class Ex5 {
    public static void main(String[] args) {
//        Khai báo biến
        int n;
        int[] number;
        int min = 0;
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);

        System.out.println("Nhập n:");
        n = sc.nextInt();
        number = new int[n];

//        chạy vòng lặp để in ra mảng số nguyên
        for (int i = 0; i < n; i++) {
            System.out.println("Nhập số nguyên:");
            number[i] = sc.nextInt();
        }

        min = number[0];
//          chạy vòng lặp để tìm min
        for (int i = 0; i < n; i++) {
            if (number[i] < min)
                min = number[i];
        }

        System.out.println("Giá trị nhỏ nhất là: " + min);
    }

}
