package exercise.exercise3;

import java.util.Scanner;

public class Ex3 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập n
        int n = sc.nextInt();

        int total = 0;
//        Chạy vòng lặp từ i đến n để tính tổng các số chẵn
        for (int i = 0; i <= n; i += 2){
            total += i;
        }
        System.out.println("Tổng các số chẵn từ 0 đến " + n + " là: " + total);
    }
}
