package exercise.exercise2;

import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập n
        int n = sc.nextInt();
//        Điều kiện để in kết quả ra màn hình
        if (n > 0){
            System.out.println("Đây là số nguyên dương");
        }else System.out.println("Đây là số nguyên âm");
    }
}
