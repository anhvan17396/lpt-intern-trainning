package exercise.exercise1;

import java.util.Scanner;

public class Ex1 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập a b
        int a = sc.nextInt();
        int b = sc.nextInt();

        int sum = a + b;
        int sub = a - b;
        int mul = a * b;
        int division = a / b;

//        In kết quả ra màn hình
        System.out.println("Tổng của 2 số là: " + sum);
        System.out.println("Hiệu của 2 số là: " + sub);
        System.out.println("Tích của 2 số là: " + mul);
        System.out.println("Thương của 2 số là: " + division);



    }
}
