package exercise.exercise4;

import java.util.Scanner;

public class Ex4 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập n
        int n = sc.nextInt();


//        Chạy vòng lặp từ 1 đến 20 rồi nhân vs n để in ra kết quả
        for (int i = 1; i <= 20 ; i++){
            System.out.println(n + " * "+ i + " = "+ (n*i));
        }

    }
}
