package object.oriented.lap28;

public class TEST {
    public static void main(String[] args) {
        Account account1 = new Account(1808, "HAI", 5000);
        account1.display();
        account1.withdraw(1900);
        account1.deposit(6000);
        System.out.println("Balance: " + account1.getBalance());
//        account1.withdraw(100);
//        System.out.println("Balance: " + account1.getBalance());
    }
}