package object.oriented.lap18;

public class ArrayCalculator {

//    Tạo maxOfArray(int[]) và maxOfArray(double[]) tìm giá trị lớn nhất
    public static int maxOfArray(int arr[]) {
        int maxValue = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > maxValue) {
                maxValue = arr[i];
            }
        }
        return maxValue;
    }

    public static double maxOfArray(double arr[]) {
        double maxValue = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > maxValue) {
                maxValue = arr[i];
            }
        }
        return maxValue;
    }

//    Tạo minOfArray(int[]) và minOfArray(double[]) tìm giá trị nhỏ nhất
    public static int minOfArray(int arr[]) {
        int minValue = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < minValue) {
                minValue = arr[i];
            }
        }
        return minValue;
    }

    public static double minOfArray(double arr[]) {
        double minValue = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < minValue) {
                minValue = arr[i];
            }
        }
        return minValue;
    }
}