package object.oriented.lap19;

public class Student {
//    khởi tạo thuộc tính
    private String name;
    private char gender;

//    khởi tạo giá trị cho thuộc tính name vs gender(khởi tạo ko tham số)
    public Student() {
        name = "Unknown";
        gender = 'u';
    }

//    khởi tạo có tham số

    public Student(String name) {
        this.name = name;
        this.gender = 'u';
    }

//    phương thức khởi tạo có tham số
    public Student(char gender) {
        this.name = "Unknown";
        this.gender = gender;
    }

//    hiển thị thông tin đối tượng
    public Student(String name, char gender) {
        this.name = name;
        this.gender = gender;
    }

//    hiển thị thông tin ra màn hình
    public void display() {
        System.out.println("Name: " + name);
        if (gender == 'u') {
            System.out.println("Gender: Unknown");
        }
        if (gender == 'm') {
            System.out.println("Gender: Male");
        }
        if (gender == 'f') {
            System.out.println("Gender: Female");
        }
    }


    public static void main(String[] args) {
        Student s1 = new Student();
        s1.display();
        Student s2 = new Student("Quang");
        s2.display();
        Student s3 = new Student('m');
        s3.display();
        Student s4 = new Student("Thu", 'f');
        s4.display();
    }
}