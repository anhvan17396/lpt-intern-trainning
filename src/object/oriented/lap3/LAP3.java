package object.oriented.lap3;
import java.awt.*;
import java.util.Scanner;


class Rectangle{
    double length, width;
    public void getInformation(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Input length: ");
        length = sc.nextDouble();
        System.out.println("Input width: ");
        width = sc.nextDouble();
    }
    public double getArea(){
        return length*width;
    }
    public double getPerimeter(){
        return (length+width)*2;
    }
    public void display(){
        System.out.println("Area: " + getArea());
        System.out.println("Perimeter: "+ getPerimeter());
    }
}
public class LAP3 {
    public static void main(String[] args) {
        Rectangle rec = new Rectangle();
        rec.getInformation();
        rec.display();

    }
}

