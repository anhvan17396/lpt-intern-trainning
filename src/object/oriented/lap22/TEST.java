package object.oriented.lap22;

public class TEST {
    public static void main(String[] args) {
        Rectangle r = new Rectangle(8, 9);
        System.out.println("Area: " + r.getArea());
        System.out.println("Perimeter: " + r.getPerimeter());
        r.setLength(2);
        r.setWidth(3);
        System.out.println("Area: " + r.getArea());
        System.out.println("Perimeter: " + r.getPerimeter());
    }
}