package com.lptech.lap49;
import java.util.Scanner;

public class LAP49 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập n
        int n = sc.nextInt();
//        Tạo vòng lặp
        for (int i = 1; i <= n; i++){
            if (n % i == 0){
                System.out.println(i + " ");
            }
        }
    }
}
