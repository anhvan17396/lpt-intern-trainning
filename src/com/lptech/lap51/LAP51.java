package com.lptech.lap51;
import java.util.Scanner;

public class LAP51 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập n
        int n = sc.nextInt();
//        chạy vòng lặp và in ra kết quả
        while (n <= 100){
            if (n % 2 == 0){
                System.out.print(n + " ");
            }
            n++;
        }
    }
}
