package com.lptech.lap43;
import java.util.Scanner;

public class LAP43 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//       Nhập a b
        int a = sc.nextInt();
        int b = sc.nextInt();
//        Tạo vòng lặp
        for (int i = a; i <= b; i++){
            System.out.print(i + " ");
        }
    }
}
