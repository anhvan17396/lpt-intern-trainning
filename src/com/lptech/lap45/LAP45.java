package com.lptech.lap45;
import java.util.Scanner;

public class LAP45 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập a b
        int a = sc.nextInt();
        int b = sc.nextInt();
        int sum = 0;
//        Tạo vòng lặp và in kết quả ra màn hình
        for (int i = a; i <= b; i++){
            sum = sum + i;
        }
        System.out.print("Total is : " + sum);

    }
}
