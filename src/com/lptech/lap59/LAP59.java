package com.lptech.lap59;
import java.util.Scanner;

public class LAP59 {
    public static void main(String[] args) {
//        tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Khai báo mảng a
        int[] arr = new int[10];
//        Chạy vòng lặp for để nhập dữ liệu cho các phần tử trong mảng và in kết quả ra màn hình
        for (int i = 0; i < 10; i++){
            arr[i] = sc.nextInt();
            System.out.print(arr[i] + " ");
        }


    }
}
