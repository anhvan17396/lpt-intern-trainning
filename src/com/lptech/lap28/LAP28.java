package com.lptech.lap28;
import java.util.Scanner;

public class LAP28 {
    public static void main(String[] args) {
//        tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập vào 2 số a b và gán giá trị cho chúng
        int a = sc.nextInt();
        int b = sc.nextInt();

//        tạo biến trung gian để hoán vị a b
        int c = b;
        b = a;
        a = c;

//        In ra màn hình kết quả theo yêu cầu
        System.out.println("After swapping, a = "+ a + " b = " + b);

    }
}
