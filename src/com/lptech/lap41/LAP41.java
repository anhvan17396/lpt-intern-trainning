package com.lptech.lap41;
import java.util.Scanner;

public class LAP41 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập tọa độ x, y
        int x = sc.nextInt();
        int y = sc.nextInt();
//        Xác định vị trí tọa độ
        if (x > 0 && y > 0){
            System.out.println("This point lies in the First quadrant");
        }else if (x < 0 && y > 0){
            System.out.println("This point lies in the Second quadrant");
        }else if (x < 0 && y < 0){
            System.out.println("This point lies in the Third quadrant");
        }else if (x > 0 && y < 0){
            System.out.println("This point lies in the Fourth quadrant");
        }else System.out.println("This point is at the ...");
    }
}
