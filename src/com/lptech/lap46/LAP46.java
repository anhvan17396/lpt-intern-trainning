package com.lptech.lap46;
import java.util.Scanner;

public class LAP46 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập n và sum
        int n = sc.nextInt();
        int sum = 0;
//        Tạo vòng lặp và in ra kết quả
        for (int i = 1; i <= n;  i += 2){
            sum = sum + i;
        }
        System.out.println("Total is: " + sum);
    }
}
