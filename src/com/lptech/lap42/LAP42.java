package com.lptech.lap42;
import java.util.Scanner;

public class LAP42 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập n
        int n = sc.nextInt();

//        Tạo vòng lặp
        for (int i = 1; i <= n; i++){
            System.out.print(i + " ");
        }
    }
}
