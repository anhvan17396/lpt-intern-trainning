package com.lptech.lap36;
import java.util.Scanner;

public class LAP36 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập a b
        int a = sc.nextInt();
        int b = sc.nextInt();
//        Kiểm tra điều kiện
        if (a != 0 && b !=0){
            System.out.println("a is not equal to 0 and b is not equal to 0");
        }else if (a == 0 && b != 0){
            System.out.println("a is equal to 0 and b is not equal to 0");
        }else if(a != 0 && b == 0){
            System.out.println("a is not equal to 0 and b is equal to 0");
        }else System.out.println("a is equal to 0 and b is equal to 0");
    }
}
