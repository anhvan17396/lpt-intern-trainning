package com.lptech.lap39;
import java.util.Scanner;

public class LAP39 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc đối tượng Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập điểm
        double n = sc.nextDouble();
//        Kiểm tra điều kiện
        if (n >= 0 && n <= 10){
            System.out.println("The score is valid");
        }else System.out.println("The score is not valid");
    }
}
