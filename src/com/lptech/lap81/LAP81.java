package com.lptech.lap81;
import java.util.Scanner;

public class LAP81 {
    public static int factorial(int n) {
        if (n == 1) return 1;
        return n * factorial(n - 1);
    }


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Nhập n= ");
        int n = sc.nextInt();
        System.out.print("Kết quả: " + factorial(n));
    }
}
