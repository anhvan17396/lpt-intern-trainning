package com.lptech.lap38;
import java.util.Scanner;

public class LAP38 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập a
        int a = sc.nextInt();
//        Kiểm tra điều kiện
        if (a >= 10 && a <= 100){
            System.out.println(a + " is in the range [10, 100]");
        }else System.out.println(a + " is not in the range [10, 100]");

    }
}
