package com.lptech.lap23;

import java.util.Scanner;

public class LAP23 {
    public static void main(String[] args) {
//        tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập vào 2 số a b và gán giá trị = sc.nextInt();
        int a = sc.nextInt();
        int b = sc.nextInt();
//        In ra màn hình kết quả tổng 2 số vừa nhập
        System.out.println("a + b = " + (a+b));
    }
}
