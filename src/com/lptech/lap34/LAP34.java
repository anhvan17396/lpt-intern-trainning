package com.lptech.lap34;
import java.util.Scanner;

public class LAP34 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập a b
        int a = sc.nextInt();
        int b = sc.nextInt();
//        Kiểm tra điều kiện
        if (a >= b){
            System.out.println("a is greater than or equal to b");
        }else System.out.println("a is smaller than b");
    }

}
