package com.lptech.lap77;
import java.util.Scanner;

public class LAP77 {
    public static void method(int[] arr){
        for (int i = 0; i < arr.length; i++){
            if (arr[i] % 3 == 0 && arr[i] % 5 != 0){
                System.out.print("Số cần tìm là: "+arr[i]+ " " );
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Nhập n = ");
        int n = sc.nextInt();
        System.out.println("Nhập mảng: ");
        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        method(arr);

    }
}
