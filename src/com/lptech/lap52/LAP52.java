package com.lptech.lap52;
import java.util.Scanner;

public class LAP52 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập n
        int n = sc.nextInt();

        int answer = 0;
//        Tạo vòng lặp và in ra kết quả
        for (int i = 1; i <= n; i++){
            if (n % i == 0){
                answer += 1;
            }
        }
        System.out.println(answer);

    }
}
