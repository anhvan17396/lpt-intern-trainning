package com.lptech.lap83;
import java.util.Scanner;

public class LAP83 {
    public static int sumOfArray(int[] arr, int n) {
        if (n == 1) return arr[0];
        return arr[n - 1] + sumOfArray(arr, n - 1);
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Nhập n= ");
        int n = sc.nextInt();
        System.out.println("Nhập mảng: ");
        int[] arr = new int[n];
        for(int i=0;i<n;i++) {
            arr[i] = sc.nextInt();
        }
        System.out.print("Tổng= "+sumOfArray(arr, n));
    }
}
