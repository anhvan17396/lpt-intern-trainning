package com.lptech.lap71;
import java.util.Scanner;

public class LAP71 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.next();
        char c = sc.next().charAt(0);
        int count = -1;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == c) {
                count = i;
                break;
//                dùng break để tìm thấy vị trí chữ cần tìm ở vị đầu tiên rồi dừng lại
            }
        }
        System.out.print(count);
    }
}
