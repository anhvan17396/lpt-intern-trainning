package com.lptech.lap76;
import java.util.Scanner;


public class LAP76 {

    public static int sumOfArray(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Nhập n = ");
        int n = sc.nextInt();
        System.out.print("Nhập mảng: ");
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        System.out.print("Tổng của mảng là: " + sumOfArray(arr));
    }

}
