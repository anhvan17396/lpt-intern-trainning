package com.lptech.lap50;


public class LAP50 {
    public static void main(String[] args) {
//        tạo 2 vòng lặp: 1 để quy đinh số lượng trên 1 dòng - 2 để hiển thị các số chạy từ a đến b
        for (int i = 0; i < 5; i++ ){
            for (int j = 0; j < 5; j++){
                System.out.print(i * 5 + j + " ");
            }
            System.out.println();
        }
    }
}
