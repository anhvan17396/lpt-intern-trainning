package com.lptech.lap27;
import java.util.Scanner;

public class LAP27 {
    public static void main(String[] args) {
//        tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        tạo 2 biến "name" và "age" và gán giá trị cho chúng
        String name = sc.next();
        int age = sc.nextInt();
//        In ra màn hình kết quả theo yêu cầu
        System.out.println("In 15 years, age of "+name+" will be "+ (age+15));
    }
}
