package com.lptech.lap78;
import java.util.Scanner;

public class LAP78 {
    public static void method(String[] arr){
        for (int i = 0; i < arr.length; i++){
            if (arr[i].length() > 3){
                System.out.println(arr[i]+ " ");
            }
        }

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Nhập n = ");
        int n = sc.nextInt();
        System.out.println("Nhập mảng: ");
        String[] arr = new String[n];

        for (int i = 0; i < n; i++) {
            arr[i] = sc.next();
        }
        method(arr);
    }

}
