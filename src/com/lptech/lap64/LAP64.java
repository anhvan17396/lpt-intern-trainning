package com.lptech.lap64;
import java.util.Scanner;

public class LAP64 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        int count = 0;

        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
            if(arr[i] % 2 != 0 && arr[i] > 0) {
                count += arr[i];
            }
        }

        System.out.print(count);
    }
}
