package com.lptech.lap82;
import java.util.Scanner;

public class LAP82 {
    public static int sum(int n) {
        if (n == 1) return 1;
        if (n % 2 == 0) {
            return sum(n - 1);
        } else {
            return n + sum(n - 1);
        }
    }


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Nhập n= ");
        int n = sc.nextInt();
        System.out.print("Tổng là: " +sum(n));
    }
}
