package com.lptech.lap22;
import java.util.Scanner;

public class LAP22 {

    public static void main(String[] args) {
//        tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);

//        Gán giá trị sc.next() cho 2 biến name vs address
        String name = sc.next();
        String address = sc.next();

//        in ra màn hình kết quả theo yêu cầu
        System.out.println("Name: "+name);
        System.out.println("Address: "+address);

    }
}
