package com.lptech.lap53;

import java.util.Scanner;

public class LAP53 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhâp a b
        int a = sc.nextInt();
        int b = sc.nextInt();
        int answer = 1;
//        Chạy vòng lặp và in kết quả ra màn hình
        while (b > 0) {
            answer *= a;
            b--;
        }
        System.out.print(answer);
    }
}