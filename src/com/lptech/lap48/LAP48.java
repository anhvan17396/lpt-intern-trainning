package com.lptech.lap48;
import java.util.Scanner;

public class LAP48 {
    public static void main(String[] args) {

//    Tạo đối tượng sc thuộc lớp Scanner
    Scanner sc = new Scanner(System.in);
//    Nhập n
    int n = sc.nextInt();

    int factorial = 1;
//    Tạo vòng lặp
    for(int i = 1; i <= n; i++){
        factorial = factorial * i;
    }
        System.out.println("The answer is: " + factorial);
}
}

