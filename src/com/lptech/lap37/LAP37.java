package com.lptech.lap37;
import java.util.Scanner;

public class LAP37 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập a b c
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
//        Kiểm tra điều kiện
        if (a >= b && b >= c){
            System.out.println(a);
        }else if (b >= c){
            System.out.println(b);
        }else System.out.println(c);
    }
}
