package com.lptech.lap21;
import java.util.Scanner;

public class LAP21 {

    public static void main(String[] args) {
//        tạo ra đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
        String name = sc.next();

//        in ra màn hình kết quả theo yêu cầu
        System.out.println("Hello "+ name);
    }
}
