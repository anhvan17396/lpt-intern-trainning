package com.lptech.lap30;
import java.util.Scanner;

public class LAP30 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập vào kí tự
        char ch;
//        In ra màn hình chữ cái tiếp theo của chữ cái đã nhập
        System.out.println("The next letter is: " +(char)(sc.next().charAt(0)+1));
    }
}
