package com.lptech.lap35;
import java.util.Scanner;

public class LAP35 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập biến "name"
        String name1 = sc.next();
        String name2 = sc.next();
//        Kiểm tra điều kiện
        if (name1.equals(name2)){
            System.out.println("two people have the same name");
        }else System.out.println("two people don't have the same name");
    }
}
