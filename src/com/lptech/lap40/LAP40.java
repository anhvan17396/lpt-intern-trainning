package com.lptech.lap40;
import java.util.Scanner;

public class LAP40 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập a b c
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
//        Kiểm tra điều kiện
        if (a <= b && b <= c){
            System.out.println("increasing");
        }else if (a >= b && b >= c){
            System.out.println("decreasing");
        }else System.out.println("neither increasing nor decreasing order");
    }
}
