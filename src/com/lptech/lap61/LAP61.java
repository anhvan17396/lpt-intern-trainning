package com.lptech.lap61;
import java.util.Scanner;

public class LAP61 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n =sc.nextInt();
        int [] arr = new int[n];

        for (int i = 0; i < n; i++){
            arr[i] = sc.nextInt();
        }
        System.out.println("Total is: " + (arr[0] + arr[n-1]));
    }
}
