package com.lptech.lap29;
import java.util.Scanner;


public class LAP29 {
    public static void main(String[] args) {
//        tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập bán kính hình tròn
        double r = sc.nextDouble();

//        In ra màn hình chu vi hình tròn
        System.out.println("Circumference = " + (2 * Math.PI * r));
    }
}
