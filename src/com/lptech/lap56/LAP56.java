package com.lptech.lap56;

public class LAP56 {
    public static void main(String[] args) {
        for (int i = 0; i <= 100; i++) {
//         chạy vòng lặp, dùng lệnh continue để in ra số %2 != 0
            if(i % 2 == 0) {
                continue;
            }
            System.out.print(i + " ");
        }
    }
}
