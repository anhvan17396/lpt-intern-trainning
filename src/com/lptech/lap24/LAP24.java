package com.lptech.lap24;
import java.util.Scanner;

public class LAP24 {
    public static void main(String[] args) {
//        tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập vào chiều dài chiều rộng của hình chữ nhật và gán trị = sc.nextInt()
        int length = sc.nextInt();
        int width = sc.nextInt();
//        In ra màn hình diện tích hình chữ nhật
        System.out.println("Area = " + (length*width));
    }
}
