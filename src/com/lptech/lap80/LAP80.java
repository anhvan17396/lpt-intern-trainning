package com.lptech.lap80;
import java.util.Scanner;

public class LAP80 {

        public static double circumference(double r) {
            return 2 * r * 3.14;
        }

        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);

            System.out.println("Nhập r= ");
            double r = sc.nextDouble();
            System.out.print("Chu vi hình tròn là: "+circumference(r));
        }
    }

