package com.lptech.lap33;
import java.util.Scanner;

public class LAP33 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhap n
        int n = sc.nextInt();
//        Kiểm tra điều kiện
        if (n > 0){
            System.out.println("n is a positive number ! ");
        }else if (n == 0){
            System.out.println("n is equal to 0 !");
        }else System.out.println("n is a negative number !");
    }
}
