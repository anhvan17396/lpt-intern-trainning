package com.lptech.lap54;
import java.util.Scanner;

public class LAP54 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập  a b
        int a = sc.nextInt();
        int b = sc.nextInt();
//        chạy vòng lặp và in ra kết quả
        for (int i = a; i <= b; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.print(i + " ");
            }
        }
    }
}
