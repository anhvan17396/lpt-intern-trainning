package com.lptech.lap70;
import java.util.Scanner;

public class LAP70 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.next();
        char c = sc.next().charAt(0);
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == c) {
                count ++;
            }
        }
        System.out.print("số lần xuất hiện của chữ trên là: "+ count);
    }
}
