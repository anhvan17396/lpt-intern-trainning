package com.lptech.lap44;
import java.util.Scanner;

public class LAP44 {
    public static void main(String[] args) {
//        Tạo đối tượng sc thuộc lớp Scanner
        Scanner sc = new Scanner(System.in);
//        Nhập n
        int n = sc.nextInt();
//        Tạo vòng lặp và in kết quả ra màn hình
        for (int i = n; i >= -n; i--){
            System.out.print(i + " ");
        }
    }
}
